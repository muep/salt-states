# Install and run the ntp daemon
#
# Known to work on these:
# - CentOS 7
# - Debian 8
# - Fedora 21
# - Fedora 22

{% if grains['os_family'] == 'RedHat' %}
{% set ntpname = 'ntpd' %}
{% elif grains['os_family'] == 'Debian' %}
{% set ntpname = 'ntp' %}
{% endif %}

ntp-package:
  pkg.installed:
    - name: ntp

ntp-service:
  service.running:
    - name: {{ ntpname }}
    - enable: True
    - watch:
      - pkg: ntp-package
