#!/bin/sh

authconfig \
  --enableldap --enableldapauth\
  --ldapserver={{ pillar['ldap-server'] }}\
  --ldapbasedn={{ pillar['ldap-basedn'] }}\
  --ldaploadcacert={{ pillar['ldap-cacert-url'] }}\
  --enablemkhomedir\
  --update
