{% if grains['os_family'] == 'RedHat' %}

ldap-logins-packages:
  pkg.installed:
    - pkgs:
      - authconfig
      - sssd-client
      - sssd-common
      - sssd-ldap

authconfig-cmd:
  cmd.script:
    - require:
      - pkg: ldap-logins-packages
    - source: salt://ldap/logins/authconfig.sh
    - template: jinja
    - unless: getent passwd {{ pillar['ldap-canary-user'] }}

sssd-service:
  service.running:
    - name: sssd
    - enable: True

{% elif grains['os_family'] == 'Debian' %}

sssd-packages:
  pkg.installed:
    - pkgs:
      - libnss-sss
      - libpam-sss
      - sssd

sssd-config:
  file.managed:
    - name: /etc/sssd/sssd.conf
    - makedirs: True
    - mode: 600
    - source: salt://ldap/logins/sssd.conf
    - template: jinja

sssd-cacert:
  file.managed:
    - name: /etc/sssd/ca.cert.pem
    - makedirs: True
    - mode: 600
    - source: {{ pillar['ldap-cacert-url'] }}
    - source_hash: {{ pillar['ldap-cacert-hash'] }}

sssd-service:
  service.running:
    - name: sssd
    - enable: True
    - watch:
      - file: sssd-config
      - file: sssd-cacert

mkhomedir:
  file.append:
    - name: /etc/pam.d/common-session
    - text: session optional pam_mkhomedir.so

{% endif %}
