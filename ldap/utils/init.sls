{% if grains['os_family'] == 'Debian' %}
{% set pkgname = 'ldap-utils' %}
{% set confdir = '/etc/ldap' %}

{% elif grains['os_family'] == 'RedHat' %}
{% set pkgname = 'openldap-clients' %}
{% set confdir = '/etc/openldap' %}

{% endif %}

ldap-utils:
  pkg.installed:
    - pkgs:
      - {{ pkgname }}


ldapconf:
  file.managed:
    - name: {{ confdir }}/ldap.conf
    - source: salt://ldap/utils/ldap.conf
    - template: jinja
    - makedirs: True

cacert:
  file.managed:
    - name: {{ confdir }}/ca.cert.pem
    - source: {{ pillar['ldap-cacert-url'] }}
    - source_hash: {{ pillar['ldap-cacert-hash'] }}
    - makedirs: True
