{% if grains['os_family'] == 'RedHat' %}
nfs-server-packages:
  pkg.installed:
    - name: nfs-utils

nfs-server-rpcbind-service:
  service.running:
    - name: rpcbind
    - enable: True
    - require:
      - pkg: nfs-server-packages

nfs-server-service:
  service.running:
    - name: nfs-server
    - enable: True
    - watch:
      - file: nfs-server-exports
    - require:
      - pkg: nfs-server-packages

{% endif %}

{% for export in pillar['nfs-exports'] %}

{% set user = export.get('user', 'root') %}
{% set group = export.get('group', 'root') %}
{% set mode = export.get('mode', '755') %}

{{ export['path'] }}:
  file.directory:
    - group: '{{ group }}'
    - user: '{{ user }}'
    - mode: '{{ mode }}'
    - makedirs: True
    - require_in:
      - service: nfs-server-service

{% endfor %}

nfs-server-exports:
  file.managed:
    - name: /etc/exports
    - source: salt://nfs-server/exports
    - template: jinja

nfs-server-firewall:
  cmd.run:
    - unless: firewall-cmd --query-service nfs
    - name: firewall-cmd --permanent --add-service nfs && firewall-cmd --reload
