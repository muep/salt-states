wlan-ap-pkgs:
  pkg.installed:
    - pkgs:
      - dnsmasq
      - NetworkManager
      - NetworkManager-wifi
      - radvd

wlan-ap-networkmanager-conf:
  file.managed:
    - name: /etc/NetworkManager/NetworkManager.conf
    - source: salt://wlan-ap/NetworkManager.conf

wlan-ap-network-manager-service:
  service.running:
    - name: NetworkManager
    - enable: True
    - watch:
      - pkg: wlan-ap-pkgs
      - file: wlan-ap-networkmanager-conf

wlan-ap-connection-conf:
  file.managed:
    - name: /etc/NetworkManager/system-connections/{{ pillar['wlan-ap-interface'] }}
    - source: salt://wlan-ap/wlan-connection
    - template: jinja
    - mode: 600
    - defaults:
        interface_name: {{ pillar['wlan-ap-interface'] }}
        ipv4_addr: {{ pillar['wlan-ap-wlan-ipv4-address'] }}
        ssid: {{ pillar['wlan-ap-ssid'] }}
        wlan_password: {{ pillar['wlan-ap-password'] }}

wlan-ap-connection-up:
  cmd.wait:
    - name: nmcli con up id wlp5s0
    - watch:
      - file: wlan-ap-connection-conf

wlan-ap-ipv4-forward:
  sysctl.present:
    - name: net.ipv4.ip_forward
    - value: 1
    - config: /etc/sysctl.d/50-salt-wlan-ap.conf

wlan-ap-ipv4-forward-filter:
  cmd.run:
    - unless: firewall-cmd --direct --query-rule ipv4 filter FORWARD 1 -j ACCEPT
    - name: firewall-cmd --direct --add-rule ipv4 filter FORWARD 1 -j ACCEPT

wlan-ap-ipv4-forward-filter-permanent:
  cmd.run:
    - unless: firewall-cmd --permanent --direct --query-rule ipv4 filter FORWARD 1 -j ACCEPT
    - name: firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 1 -j ACCEPT

wlan-ap-ipv6-forward:
  sysctl.present:
    - name: net.ipv6.conf.all.forwarding
    - value: 1
    - config: /etc/sysctl.d/50-salt-wlan-ap.conf

wlan-ap-ipv6-forward-filter:
  cmd.run:
    - unless: firewall-cmd --direct --query-rule ipv6 filter FORWARD 1 -j ACCEPT
    - name: firewall-cmd --direct --add-rule ipv6 filter FORWARD 1 -j ACCEPT

wlan-ap-ipv6-forward-filter-permanent:
  cmd.run:
    - unless: firewall-cmd --permanent --direct --query-rule ipv6 filter FORWARD 1 -j ACCEPT
    - name: firewall-cmd --permanent --direct --add-rule ipv6 filter FORWARD 1 -j ACCEPT

wlan-ap-dnsmasq-conf:
  file.managed:
    - name: /etc/dnsmasq.conf
    - source: salt://wlan-ap/dnsmasq.conf
    - template: jinja
    - defaults:
        dhcp_range: {{ pillar['wlan-ap-dnsmasq-dhcp-range'] }}
        domain: {{ pillar['wlan-ap-wlan-domain'] }}
        interface: {{ pillar['wlan-ap-interface'] }}

wlan-ap-dnsmasq-service:
  service.running:
    - name: dnsmasq
    - enable: True
    - watch:
      - pkg: wlan-ap-pkgs
      - file: wlan-ap-dnsmasq-conf


{% for s in ('dns', 'dhcp') %}

wlan-ap-allow-{{ s }}-in-config:
  cmd.run:
    - unless: firewall-cmd --permanent --query-service {{ s }}
    - name: firewall-cmd --permanent --add-service {{ s }}

wlan-ap-allow-{{ s }}-runtime:
  cmd.run:
    - unless: firewall-cmd --query-service {{ s }}
    - name: firewall-cmd --add-service {{ s }}

{% endfor %}

wlan-ap-radvd-conf:
  file.managed:
    - name: /etc/radvd.conf
    - source: salt://wlan-ap/radvd.conf
    - template: jinja
    - defaults:
        interface: {{ pillar['wlan-ap-interface'] }}
        prefix: {{ pillar['wlan-ap-wlan-ipv6-network'] }}

wlan-ap-radvd-service:
  service.running:
    - name: radvd
    - enable: True
    - watch:
      - pkg: wlan-ap-pkgs
      - file: wlan-ap-radvd-conf
