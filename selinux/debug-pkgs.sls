# selinux/debug-pkgs.sls
#
# Things that are typically useful for diagnosing SELinux-related
# issues. Currently does nothing on distributions that are not
# Fedora derivatives.
{% if grains['os_family'] == 'RedHat' %}

{% if grains['os'] == 'Fedora' %}
{% set manpkg_name = 'selinux-policy-doc' %}
{% set semanage_pkg = 'policycoreutils-python-utils' %}
{% else %}
{% set manpkg_name = 'selinux-policy-devel' %}
{% set semanage_pkg = 'policycoreutils-python' %}
{% endif %}

selinux-debug-pkgs:
  pkg.installed:
    - pkgs:
       - checkpolicy
       - {{ manpkg_name }}
       - {{ semanage_pkg }}
       - setools-console

{% endif %}
