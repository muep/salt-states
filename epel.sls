# epel.sls
#
# A guite trivial state that just enables the
# EPEL repository on CentOS 7.
#
# Does not currently work on other distributions
# where EPEL in theory could be used.

{% if grains['osfinger'] == 'CentOS Linux-7' %}
epel-release:
  pkg.installed: []
{% endif %}
