# postgresql/server.sls
#
# State for installing and running a simple postgresql server.
# Currently only works with Fedora and its derivatives, like
# CentOS.

{% if grains['os_family'] == 'RedHat' %}

postgresql-server-pkg:
  pkg.installed:
    - name: postgresql-server

postgresql-initdb:
  cmd.run:
    - name: postgresql-setup initdb
    - unless: test -f /var/lib/pgsql/data/PG_VERSION
    - require:
      - pkg: postgresql-server-pkg

postgresql-service:
  service.running:
    - name: postgresql
    - enable: True
    - watch:
      - pkg: postgresql-server-pkg
      - cmd: postgresql-initdb

{% endif %}
