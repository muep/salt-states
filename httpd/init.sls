{% if grains['os_family'] == 'RedHat' %}
{% set httpd_pkg_name = 'httpd' %}
{% set httpd_service_name = 'httpd' %}
{% elif grains['os_family'] == 'Debian' %}
{% set httpd_pkg_name = 'apache2' %}
{% set httpd_service_name = 'apache2' %}
{% endif %}

httpd-pkg:
  pkg.installed:
    - pkgs:
      - {{ httpd_pkg_name }}

httpd-service:
  service.running:
    - name: {{ httpd_service_name }}
    - enable: True
    - watch:
      - pkg: httpd-pkg

{% if grains['os_family'] == 'RedHat' %}
/etc/httpd/conf.d/welcome.conf:
  file.managed:
    - source: salt://httpd/welcome.conf
    - require:
      - pkg: httpd-pkg
    - watch_in:
      - service: httpd-service

allow-http:
  cmd.run:
    - unless: firewall-cmd --permanent --query-service http
    - name: firewall-cmd --add-service http; firewall-cmd --permanent --add-service http
{% endif %}
