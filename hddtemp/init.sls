# Install hddtemp and run the service so that hard disk temperatures
# get periodically stored in syslog.
#
# Known to work on these:
# - CentOS 7
# - Fedora 21
# - Fedora 22
#
# Might be reasonable to use like this in top.sls of state tree:
#
# base:
#   'virtual:physical':
#     - match: grain
#     - hddtemp

hddtemp-package:
  pkg.installed:
    - name: hddtemp

hddtemp-service:
  service.running:
    - name: hddtemp
    - enable: True
    - watch:
      - pkg: hddtemp-package

{% if grains['os_family'] == 'RedHat' %}
hddtemp-sysconfig-file:
  file.replace:
    - name: /etc/sysconfig/hddtemp
    - pattern: '^HDDTEMP_OPTIONS=.*$'
    - repl: 'HDDTEMP_OPTIONS="-l 127.0.0.1 -S 600"'
    - append_if_not_found: True
    - show_changes: True
    - watch:
      - pkg: hddtemp-package
    - watch_in:
      - service: hddtemp-service
{% endif %}
