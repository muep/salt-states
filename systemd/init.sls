{% if grains['init'] == 'systemd' %}

systemd-journal-dir:
  file.directory:
    - name: /var/log/journal

systemd-splitmode-none:
  file.replace:
    - name: /etc/systemd/journald.conf
    - pattern: '^SplitMode=.*'
    - repl: 'SplitMode=none'
    - append_if_not_found: True

{% if 'systemd-journal-systemmaxuse' in pillar %}
systemd-journal-systemmaxuse:
  file.replace:
    - name: /etc/systemd/journald.conf
    - pattern: '^SystemMaxUse=.*'
    - repl: 'SystemMaxUse={{ pillar['systemd-journal-systemmaxuse'] }}'
    - append_if_not_found: True
{% endif %}

{% endif %}
