{% set version = 'ee4c4602726a1d1064c96414a8e4df0308f3be62' %}
{% set installdir = '/var/www/tt-rss-' + version %}

include:
  - httpd
  - postgresql.server

tt-rss-deps:
  pkg.installed:
    - pkgs:
      - php
      - php-gd
      - php-mbstring
      - php-pgsql
      - php-xml
      - policycoreutils-python
    - watch_in:
      - service: httpd-service

tt-rss-selinux:
  cmd.script:
    - unless: semanage fcontext -l | grep {{ installdir }}
    - source: salt://tt-rss/adjust-selinux.sh
    - template: jinja
    - context:
        installdir: {{ installdir }}
    - require:
      - pkg: tt-rss-deps

tt-rss-files:
  archive.extracted:
    - name: /var/www
    - if_missing: {{ installdir }}
    - source: salt://tt-rss/tt-rss-{{ version }}.tar.xz
    - archive_format: tar
    - tar_options: J
    - require:
      - cmd: tt-rss-selinux

# No idea on why this is required
tt-rss-restorecon:
  cmd.wait:
    - name: restorecon -R {{ installdir }}
    - watch:
      - archive: tt-rss-files

tt-rss-chown:
  cmd.wait:
    - name: chown -R apache:apache {{ installdir }}
    - watch:
      - archive: tt-rss-files

tt-rss-httpd-conf:
  file.managed:
    - name: /etc/httpd/conf.d/tt-rss.conf
    - source: salt://tt-rss/httpd-tt-rss.conf
    - template: jinja
    - context:
        installdir: {{ installdir }}
    - watch_in:
      - service: httpd-service

tt-rss-config-php:
  file.managed:
    - name: {{ installdir }}/config.php
    - source: salt://tt-rss/tt-rss-config.php
    - template: jinja
    - require:
      - archive: tt-rss-files

tt-rss-postgres-user:
  postgres_user.present:
    - name: tt-rss
    - login: True
    - encrypted: True
    - password: {{ pillar['tt-rss-database-password'] }}

tt-rss-postgres-db:
  postgres_database.present:
    - name: tt-rss
    - owner: tt-rss
    - require:
      - postgres_user: tt-rss-postgres-user

tt-rss-db-initialize:
  cmd.wait:
    - name: psql tt-rss tt-rss < {{ installdir }}/schema/ttrss_schema_pgsql.sql
    - env:
      - PGPASSWORD: {{ pillar['tt-rss-database-password'] }}
    - watch:
      - postgres_database: tt-rss-postgres-db

tt-rss-pg-hba:
  file.prepend:
    - name: /var/lib/pgsql/data/pg_hba.conf
    - text:
      - local tt-rss tt-rss md5

    - watch_in:
      - service: postgresql-service

tt-rss-update-unit:
  file.managed:
    - name: /etc/systemd/system/tt-rss-update.service
    - source: salt://tt-rss/tt-rss-update.service
    - template: jinja
    - context:
        installdir: {{ installdir }}

tt-rss-systemd-daemon-reload:
  cmd.wait:
    - name: systemctl daemon-reload
    - watch:
      - file: tt-rss-update-unit

tt-rss-update-service:
  service.running:
    - name: tt-rss-update
    - enable: True
    - watch:
      - file: tt-rss-update-unit
    - require:
      - cmd: tt-rss-systemd-daemon-reload
      - service: postgresql-service
      - cmd: tt-rss-restorecon
      - file: tt-rss-config-php
