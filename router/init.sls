{% set iface_lan = pillar['router_interface'] %}
{% set iface_wan = iface_lan + '.2' %}

router-pkgs:
  pkg.installed:
    - pkgs:
      - conntrack-tools
      - dnsmasq
      - iptables
      - iptables-services

router-nonpkgs:
  pkg.purged:
    - pkgs:
      - firewalld

firewalld-not-running:
  service.dead:
    - name: firewalld
    - enable: False

ifcfg_base:
  file.managed:
    - name: /etc/sysconfig/network-scripts/ifcfg-{{ iface_lan }}
    - source: salt://router/ifcfg-eth
    - template: jinja

ifcfg_vlan:
  file.managed:
    - name: /etc/sysconfig/network-scripts/ifcfg-{{ iface_wan }}
    - source: salt://router/ifcfg-eth.2
    - template: jinja

networkmanager-service:
  service.running:
    - name: NetworkManager
    - enable: True
    - watch:
      - file: ifcfg_base
      - file: ifcfg_vlan

hosts:
  file.managed:
    - name: /etc/hosts
    - source: salt://router/hosts
    - template: jinja

dnsmasq_conf:
  file.managed:
    - name: /etc/dnsmasq.d/{{ pillar['network_domain'] }}.conf
    - source: salt://router/dnsmasq-salt.conf
    - template: jinja

dnsmasq:
  service.running:
    - enable: True
    - watch:
      - file: dnsmasq_conf
      - file: hosts
      - pkg: router-pkgs

iptables-config:
  file.managed:
    - name: /etc/sysconfig/iptables
    - source: salt://router/iptables.conf
    - template: jinja
    - context:
        iface_lan: {{ iface_lan }}
        iface_wan: {{ iface_wan }}
        lan_ipv4_space: {{ pillar['lan-ipv4-space'] }}

ip6tables-config:
  file.managed:
    - name: /etc/sysconfig/ip6tables
    - source: salt://router/ip6tables.conf
    - template: jinja
    - context:
        iface_lan: {{ iface_lan }}
        iface_wan: {{ iface_wan }}
        lan_ipv6_space: {{ pillar['lan-ipv6-space'] }}

iptables-service:
  service.running:
    - name: iptables
    - enable: True
    - watch:
      - file: iptables-config

ip6tables-service:
  service.running:
    - name: ip6tables
    - enable: True
    - watch:
      - file: ip6tables-config

router-ipv4-forwarding:
  sysctl.present:
    - name: net.ipv4.ip_forward
    - value: 1

router-ipv6-forwarding:
  sysctl.present:
    - name: net.ipv6.conf.all.forwarding
    - value: 1
